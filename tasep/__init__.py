#!/usr/bin/env python
# coding: utf-8

"""TASEP simulation"""

import numpy as np
import atooms
from atooms.system import Particle, System, Cell
from atooms.trajectory import TrajectoryXYZ


def initial_system(N=20, L=100):

    """Setup initial system"""

    system = System()

    # Add particles and assign positions
    # (one next to the other to start with)
    system.particle = [Particle() for i in range(N)]
    for i, particle in enumerate(system.particle):
        particle.position = i
        particle.radius = 1
        particle.velocity = None

    # Define the lattice of length L
    system.cell = Cell([L])
    return system

def particle_distance(p1, p2, cell):

    """Compute the distance between two consecutive particles within cell"""

    if p2.position > p1.position:
        d = p2.position - p1.position
    else:
        d = p2.position + int(cell.side) - p1.position
    return d


class TASEP(object):

    """Backend to simulate a TASEP"""

    def __init__(self, system):
        self.system = system
        self.time = 0.0

        # Homogenous hopping rate p, each site has its rate
        p = 1.
        system.cell.rates = [p for i in range(int(system.cell.side))]

        # Propensities used for the Gillespie dynamics
        # TODO: We can keep it as a list or add it as a property of each particle (maybe easier?)
        def propensities(system):
            # TODO: can be streamlined
            a = [0. for i in range(len(system.particle))]
            # initialise the propensities
            p_id = 0
            while p_id < len(system.particle):
                p = system.particle[p_id]
                p_next = system.particle[(p_id+1) % len(system.particle)]
                d = particle_distance(p, p_next, system.cell)
                if d > p.radius :
                    a[p_id] = system.cell.rates[p.position]
                p_id += 1
            return a

        self.propensity = propensities(self.system)

    def _move_particle(self, i):
        system = self.system  # alias

        # Move a particle to the next site
        system.particle[i].position = (system.particle[i].position + 1) % int(system.cell.side)

        # Update the propensity of the the moving particle
        p = system.particle[i]
        p_next = system.particle[(i+1) % len(system.particle)]
        d = particle_distance(p, p_next, system.cell)
        if d > p.radius:
            self.propensity[i] = system.cell.rates[p.position]
        else :
            self.propensity[i] = 0.

        # Update the propensity of the the previous particle
        p = system.particle[i-1]
        p_next = system.particle[i]
        d = particle_distance(p, p_next, system.cell)
        if d > p.radius :
            self.propensity[i-1] = system.cell.rates[p.position]

    def run(self, steps):
        """
        Perform `steps` iterations of Gillespie dynamics. The actual time
        is stored in the `time` instance variable.
        """
        from random import seed, random, uniform
        from bisect import bisect
        from math import log

        for i in range(steps):
            # First compute Q[]
            Q = []
            cum = 0.
            for prop in self.propensity:
                cum += prop
                Q.append(cum)

            # Then chose the particle moving (reaction)
            ran1 = uniform(0, Q[-1])

            # Binary search
            reaction = bisect(Q, ran1)

            # Move the particle
            self._move_particle(int(reaction))

            # Compute the dt of the step
            ran2 = random()
            dt = -log(ran2) / Q[-1]

            self.time += dt


def center(system):
    """
    Return a centered copy of the system
    """
    from copy import deepcopy
    system_center = System(deepcopy(system.particle), system.cell)
    for p in system_center.particle:
        p.position -= system.cell.side / 2.0
    return system_center

def write_config(sim, fields=None, precision=None):
    """
    Write configurations to a trajectory file.

    The trajectory format is taken from the passed Simulation
    instance.
    """
    from atooms.core.utils import rmd, rmf
    if sim.current_step == 0:
        # TODO: folder-based trajectories should ensure that mode='w' clears up the folder
        rmd(sim.output_path)
        rmf(sim.output_path)

    with sim.trajectory(sim.output_path, 'a') as t:
        if precision is not None:
            t.precision = precision
        if fields is not None:
            t.fields = fields
        t.write(center(sim.system), sim.backend.time)

def show(sim, only=None):
    line = [' '] * sim.system.cell.side[0]
    if only is not None:
        for i in only:
            line[sim.system.particle[i].position] = '+'
    else:
        for i, p in enumerate(sim.system.particle):
            line[sim.system.particle[i].position] = '+'
    print(''.join(line))

if __name__ == '__main__':

    import random
    from atooms.trajectory import TrajectoryXYZ
    from atooms.simulation import Simulation

    random.seed(1)
    system = initial_system()
    backend = TASEP(system)
    backend.trajectory = TrajectoryXYZ
    simulation = Simulation(backend, output_path='/tmp/test.xyz')
    simulation.add(write_config, 100)
    #simulation.add(show, 50, only=[1, 2])  # show only two partciles
    simulation.run(100000)
    print('Time after run: %.3f' % backend.time + ' s\n')
