import numpy
import re
import logging
from atooms.core.utils import tipify
from atooms.system.particle import Particle, distinct_species
from atooms.system.cell import Cell
from atooms.system import System
from atooms.trajectory import TrajectoryXYZ as ttt
from atooms.trajectory import Unfolded

class TrajectoryXYZ(ttt):

    def _read_metadata(self, frame):
        """
        Internal xyz method to get header metadata from comment line of
        given `frame`.

        We assume metadata fmt is a space-separated sequence of comma
        separated entries such as:

        columns:id,x,y,z step:10
        columns=id,x,y,z step=10
        """
        import re 

        # Go to line and skip Npart info
        self.trajectory.seek(self._index_header[frame])
        npart = int(self.trajectory.readline())
        data = self.trajectory.readline()

        # TODO: accept "text text" entries
        # TODO: accept extended xyz format
        # Remove spaces around : or = and replace = by :
        data = re.sub(r'\s*[=:]\s*', ':', data)

        # Fill metadata dictionary
        meta = {}
        meta['npart'] = npart
        for e in data.split():
            s = re.search(r'(\S+):(\S+)', e)            
            if s is not None:
                tag, data = s.group(1), s.group(2)
                # Remove dangling commas
                data = data.strip(',')
                # If there are commas, this is a list, else a scalar.
                # We convert the string to appropriate types
                if ',' in data:
                    meta[tag] = [tipify(_) for _ in data.split(',')]
                else:
                    meta[tag] = tipify(data)

        # Apply an alias dict to tags, e.g. to add step if Cfg was found instead
        for alias, tag in self.alias.items():
            try:
                meta[tag] = meta[alias]
            except KeyError:
                pass

        # Fix dimensions based on side of cell.
        # Fallback to ndim metadata or 3.
        try:
            if 'ndim' not in meta:
                meta['ndim'] = len(meta['cell'])
        except TypeError:
            meta['ndim'] = 1  # it is a scalar
        except KeyError:
            meta['ndim'] = 3  # default

        # Make sure columns is a list
        if 'columns' in meta:
            if not isinstance(meta['columns'], list):
                meta['columns'] = [meta['columns']]

        return meta

import sys

fileinp = sys.argv[1]
trajectory = Unfolded(TrajectoryXYZ(fileinp))
j0 = 0
Nsites = trajectory[j0].cell.side
initial = trajectory[j0]
for j, system in enumerate(trajectory):
    Q = 0
    for i in range((len(system.particle))):
        Q += abs(system.particle[i].position[0] - initial.particle[i].position[0])
T = trajectory.times[j] - trajectory.times[j0]
print(Q, Q / T, Q / T / Nsites)
                   
