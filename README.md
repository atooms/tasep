TASEP
=====

Atooms implementation of a totally asymmetric simple exclusion process (TASEP)

Quick start
-----------

```python
from tasep import initial_system, TASEP, show
system = initial_system()
backend = TASEP(system)
simulation = Simulation(backend)
simulation.add(show, 50, only=[1, 2])
simulation.run(1000)
```

Installation
------------
The easiest way to install tasep is with pip
```
pip install tasep
```

Alternately, you can clone the code repository and install from source
```
git clone https://gitlab.info-ufr.univ-montp2.fr/atooms/tasep.git
cd tasep
make install
```

Authors
-------
Luca Ciandrini: http://www.coulomb.univ-montp2.fr/perso/luca.ciandrini/

Daniele Coslovich: http://www.coulomb.univ-montp2.fr/perso/daniele.coslovich/
