#!/usr/bin/env python

from __future__ import print_function

import unittest
import numpy
from tasep import TASEP, initial_system
from atooms.simulation import Simulation


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def test_density(self):
        """Test local density"""
        def density(sim, db):
            for p in sim.system.particle:
                db[p.position] += 1
                
        steps = 10000
        system = initial_system()
        db = [0] * int(system.cell.side)
        backend = TASEP(system)
        simulation = Simulation(backend)
        simulation.add(density, 50, db)
        simulation.run(steps)
        db = numpy.array(db, dtype=numpy.float64)
        norm = len(system.particle) * (float(steps)/50)
        db /= norm
        self.assertLess(abs(numpy.mean(db) - 0.01), 1e-4)


    def test_current(self):
        import random
        from atooms.trajectory import Unfolded, TrajectoryRam
        from tasep import write_config, center
                
        random.seed(1)
        def write_config(sim):
            if sim.current_step == 0:
                sim._trajectory = TrajectoryRam()
            sim._trajectory.write(center(sim.system), sim.backend.time)
            
        for rho in [0.1, 0.4, 0.5, 0.6, 0.9]:
            L = 100
            steps = L * 150
            system = initial_system(N=int(rho * L), L=L)
            backend = TASEP(system)
            backend.trajectory = TrajectoryRam
            simulation = Simulation(backend)
            simulation.add(write_config, 100)
            simulation.run(steps)

            trajectory = Unfolded(simulation._trajectory)
            j0 = int(len(trajectory) * 0.3)
            Nsites = trajectory[j0].cell.side[0]
            initial = trajectory[j0]
            for j in range(j0, len(trajectory)):
                system = trajectory[j]
                Q = 0
                for i in range((len(system.particle))):
                    Q += abs(system.particle[i].position[0] - initial.particle[i].position[0])
                    #print(trajectory.times[j], i, system.particle[i].position[0])
            T = trajectory.times[j] - trajectory.times[j0]
            #print(rho, T, Q, Q/T, Q/T/Nsites)
            self.assertLess(abs(Q/T/Nsites) - rho*(1-rho), 2e-2)


if __name__ == '__main__':
    unittest.main()


